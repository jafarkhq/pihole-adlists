My PiHole Adlists


| **Name**                                        | **Host**                                                                                | **null** |
|-------------------------------------------------|-----------------------------------------------------------------------------------------|----------|
| StevenBlack                                     | https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts                        |          |
| WindowsSpyBlocker                               | https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt |          |
| dbl-oisd-nl                                     | https://raw.githubusercontent.com/ookangzheng/dbl-oisd-nl/master/dbl.txt                |          |
| Developerdan Ads & Tracking                     | https://www.github.developerdan.com/hosts/lists/ads-and-tracking-extended.txt           |          |
| Developerdan Facebook                           | https://www.github.developerdan.com/hosts/lists/facebook-extended.txt                   |          |
| Energized Ultimate                              | https://block.energized.pro/ultimate/formats/hosts                                      |          |
| Energized Porn                                  | https://block.energized.pro/porn/formats/hosts                                          |          |
| StevenBlack Unified hosts + fakenews + gambling | http://sbc.io/hosts/alternates/fakenews-gambling/hosts                                  |          |
| Energized Regional                              | https://block.energized.pro/extensions/regional/formats/hosts                           |          |
| tiktok                                          | https://raw.githubusercontent.com/llacb47/mischosts/master/social/tiktok-block          |          |
| fingerprinting  White Ops                       | https://raw.githubusercontent.com/llacb47/mischosts/master/whiteops-hosts               |          |
| KADomains                                       | https://raw.githubusercontent.com/PolishFiltersTeam/KADhosts/master/KADomains.txt       |          |
| Developerdan Dating Services                    | https://www.github.developerdan.com/hosts/lists/dating-services-extended.txt            |          |
| Developerdan Hate & Junk                        | https://www.github.developerdan.com/hosts/lists/hate-and-junk-extended.txt              |          |
| My DoH Domains                                  | https://gitlab.com/jafarkhq/pihole-adlists/-/raw/main/adlists/doh                       |          |
| Domain Squatting                                | https://raw.githubusercontent.com/RPiList/specials/master/Blocklisten/DomainSquatting   |          |
| fb, insta, whatsapp                             | https://raw.githubusercontent.com/jmdugan/blocklists/master/corporations/facebook/all   |          |
| DoH Domains                                     | https://raw.githubusercontent.com/dibdot/DoH-IP-blocklists/master/doh-domains.txt       |          |
| My findings                                     | https://gitlab.com/jafarkhq/pihole-adlists/-/raw/main/adlists/random                    |          |
